import { ApiProperty } from '@nestjs/swagger';

export class CreateRoleDto {
  @ApiProperty({ example: 'ADMIN', description: 'Название роли пользователя' })
  readonly value: string;
  @ApiProperty({ example: 'Описание...', description: 'Описание роли' })
  readonly description: string;
}
