import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
// DTO
import { CreateRoleDto } from './dto/create-role.dto';

// МОДЕЛИ БД
import { Role } from './roles.model';

@Injectable()
export class RolesService {
  constructor(@InjectModel(Role) private roleRepository: typeof Role) {}

  async createRole(dto: CreateRoleDto) {
    const role = await this.roleRepository.create(dto);
    return role;
  }

  async getAllRoles() {
    const roles = await this.roleRepository.scope('allRoles').findAll();
    return roles;
  }

  async getRoleByValue(value: string) {
    const role = await this.roleRepository.findOne({ where: { value } });
    return role;
  }
}
