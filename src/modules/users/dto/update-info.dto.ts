import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString, IsDateString } from 'class-validator';

export class UpdateUserDto {
  @ApiProperty({ example: 'Иван', description: 'имя' })
  @IsOptional()
  @IsString({ message: 'Должно быть строкой' })
  readonly name?: string;

  @ApiProperty({
    example: '2000-08-21',
    description: 'Дата рождения',
  })
  @IsOptional()
  @IsDateString()
  readonly birthDate?: Date;

  @ApiProperty({ example: 'Немного о себе', description: 'Инофрмация о себе' })
  @IsOptional()
  @IsString({ message: 'Должно быть строкой' })
  readonly aboutYourself?: string;
}
