import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsEmail, IsString, Length } from 'class-validator';

export class CreateUserDto {
  @ApiProperty({ example: 'user@mail.ru', description: 'Почта' })
  @IsString({ message: 'Должно быть строкой' })
  @IsEmail({}, { message: 'Некорректный email' })
  readonly email: string;
  @ApiProperty({ example: '12345', description: 'пароль' })
  @IsString({ message: 'Должно быть строкой' })
  @Length(4, 16, { message: 'Не меньше 4 и не больше 16' })
  readonly password: string;
  @ApiProperty({ example: 'Иван', description: 'имя' })
  @IsString({ message: 'Должно быть строкой' })
  readonly name: string;
  @ApiProperty({ example: 'Немного о себе', description: 'Инофрмация о себе' })
  @IsString({ message: 'Должно быть строкой' })
  aboutYourself: string;
  @ApiProperty({ example: '2000-08-21', description: 'Дата рождения' })
  @IsDateString()
  readonly birthDate: Date;
}
