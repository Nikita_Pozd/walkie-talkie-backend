import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';

import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

// МОДЕЛИ БД
import { User } from './users.model';

// GUARDS
// import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { RolesGuard } from '../auth/roles.guard';

// ДЕКОРАТОРЫ
import { Roles } from '../auth/roles-auth.decorator';

// СЕРВИСЫ
import { UsersService } from './users.service';

// DTO
import { CreateUserDto } from './dto/create-user.dto';
import { AddRoleDto } from './dto/add-role.dto';
import { BanUserDto } from './dto/ban-user.dto';
import { UpdateUserDto } from './dto/update-info.dto';
import { CheckEmailDto } from './dto/check-email.dto';

// PARAMS
import { AddRoleParams } from './params/add-role.params';
import { UpdateInfoParams } from './params/update-info.params';
import { RemoveUserParams } from './params/remove-user.params';
import { BanUserParams } from './params/ban-user.params';
import { defaultRoles } from 'src/enums/defaultRoles.enum';

@ApiTags('Пользователи')
@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @ApiOperation({ summary: 'Создание пользователя' })
  @ApiResponse({ status: 201, type: User })
  @Post()
  async create(@Body() userDto: CreateUserDto) {
    return await this.usersService.createUser(userDto);
  }

  @ApiOperation({ summary: 'Получить всех пользователей' })
  @ApiResponse({ status: 200, type: [User] })
  // @Roles('ADMIN')
  // @UseGuards(RolesGuard)
  @Get()
  async getAll() {
    return await this.usersService.getAllUsers();
  }

  @ApiOperation({ summary: 'Изменение пользователя' })
  @ApiResponse({ status: 200, type: User })
  // @Roles('USER')
  // @UseGuards(RolesGuard)
  @Put(':userId')
  async updateInfo(
    @Param() params: UpdateInfoParams,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    return await this.usersService.updateUserData(params.userId, updateUserDto);
  }

  @ApiOperation({ summary: 'Удалить пользователя' })
  // @Roles(defaultRoles.ADMIN)
  // @UseGuards(RolesGuard)
  @Delete(':userId')
  async removeUser(@Param() params: RemoveUserParams) {
    return await this.usersService.removeUser(params.userId);
  }

  @ApiOperation({ summary: 'Проверить существование почты' })
  @ApiResponse({ status: 200, type: Boolean })
  @Get('/check/isEmailUsed')
  async checkEmail(@Query() dto: CheckEmailDto) {
    return await this.usersService.isEmailAlreadyUsed(dto.email);
  }

  @ApiOperation({ summary: 'Выдать роль пользователю' })
  @ApiResponse({ status: 200 })
  // @Roles(defaultRoles.ADMIN)
  // @UseGuards(RolesGuard)
  @Post('/:userId/addRole')
  async addRole(@Param() params: AddRoleParams, @Body() dto: AddRoleDto) {
    return await this.usersService.addRole(params.userId, dto);
  }

  @ApiOperation({ summary: 'Забанить пользователя' })
  @ApiResponse({ status: 200 })
  @Roles('ADMIN')
  @UseGuards(RolesGuard)
  @Post('/:userId/ban')
  async ban(@Param() params: BanUserParams, @Body() dto: BanUserDto) {
    return await this.usersService.ban(params.userId, dto);
  }
}
