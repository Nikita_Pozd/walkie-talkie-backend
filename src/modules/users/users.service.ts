import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as bcrypt from 'bcryptjs';
import { InjectModel } from '@nestjs/sequelize';

// МОДЕЛИ БД
import { User } from './users.model';

// DTO
import { CreateUserDto } from './dto/create-user.dto';
import { AddRoleDto } from './dto/add-role.dto';
import { BanUserDto } from './dto/ban-user.dto';
import { UpdateUserDto } from './dto/update-info.dto';

// СЕРВИСЫ
import { RolesService } from '../roles/roles.service';
import { defaultRoles } from 'src/enums/defaultRoles.enum';

@Injectable()
export class UsersService {
  private readonly saltRounds = 10;

  constructor(
    @InjectModel(User) private userRepository: typeof User,
    private roleService: RolesService,
  ) {}

  // СОЗДАТЬ ХЭШ ИЗ ПАРОЛЯ
  private async hashPassword(password: string): Promise<string> {
    const salt = await bcrypt.genSalt(this.saltRounds);
    return await bcrypt.hash(password, salt);
  }

  // СОЗДАТЬ ПОЛЬЗОВАТЕЛЯ
  async createUser(dto: CreateUserDto) {
    const candidate = await this.getUserByEmail(dto.email);
    if (candidate) {
      throw new HttpException(
        'Пользователь с таким email существует',
        HttpStatus.BAD_REQUEST,
      );
    }
    const hashPassword = await this.hashPassword(dto.password);
    const user = await this.userRepository.create({
      ...dto,
      password: hashPassword,
    });
    const role = await this.roleService.getRoleByValue(defaultRoles.USER);
    await user.$set('roles', [role.id]);
    const resultUser = await this.getUserById(user.id);

    return resultUser;
  }

  // ОБНОВИТЬ ПОЛЯ ПОЛЬЗОВАТЕЛЯ
  async updateUserData(id: number, dto: UpdateUserDto) {
    return await this.userRepository.update(dto, { where: { id: id } });
  }

  // ПРОВЕРИТЬ, СУЩЕСТВУЕТ ЛИ УЖЕ ПОЧТА
  async isEmailAlreadyUsed(email: string): Promise<boolean> {
    const candidate = await this.userRepository.findOne({
      where: { email },
      include: { all: true },
    });
    return candidate !== null;
  }

  // ПОЛУЧИТЬ СПИСОК ВСЕХ ПОЛЬЗОВАТЕЛЕЙ
  async getAllUsers() {
    const users = await this.userRepository
      .scope(['defaultScope', 'withRole', 'allUsers'])
      .findAll();
    return users;
  }

  // УДАЛИТЬ ПОЛЬЗОВАТЕЛЕЯ
  async removeUser(id: number) {
    return await this.userRepository.destroy({ where: { id: id } });
  }

  // ПОЛУЧИТЬ ПОЛЬЗОВАТЕЛЯ ПО ПОЧТЕ
  async getUserByEmail(email: string) {
    const user = await this.userRepository
      .scope(['defaultScope', 'withRole'])
      .findOne({
        where: { email },
      });
    return user;
  }

  // ПОЛУЧИТЬ ПОЛЬЗОВАТЕЛЯ ПО ID
  async getUserById(id: number) {
    const user = await this.userRepository
      .scope(['defaultScope', 'withRole'])
      .findOne({
        where: { id },
      });
    return user;
  }

  // ДОБАВИТЬ РОЛЬ ПОЛЬЗОВАТЕЛЮ
  async addRole(id: number, dto: AddRoleDto) {
    const user = await this.userRepository.findByPk(id);
    const role = await this.roleService.getRoleByValue(dto.value);
    if (role && user) {
      await user.$add('role', role.id);
      return dto;
    }
    throw new HttpException(
      'Пользователь или роль не найдены',
      HttpStatus.NOT_FOUND,
    );
  }

  // ЗАБАНИТЬ ПОЛЬЗОВАТЕЛЯ
  async ban(id: number, dto: BanUserDto) {
    const user = await this.userRepository.findByPk(id);
    if (!user) {
      throw new HttpException('Пользователь не найден', HttpStatus.NOT_FOUND);
    }
    user.isBanned = true;
    user.banReason = dto.banReason;
    await user.save();
    return user;
  }
}
