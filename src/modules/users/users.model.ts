import {
  BelongsToMany,
  Column,
  DataType,
  DefaultScope,
  HasMany,
  Model,
  Scopes,
  Table,
  CreatedAt,
  UpdatedAt,
  Sequelize,
} from 'sequelize-typescript';
import { ApiProperty } from '@nestjs/swagger';

// МОДЕЛИ БД
import { Role } from '../roles/roles.model';
import { UserRoles } from '../roles/user-roles.model';
import { Post } from '../posts/posts.model';

interface UserCreationAttrs {
  email: string;
  name: string;
  password: string;
}

interface UserAttrs {
  id: number;
  email: string;
  password: string;
  name: string;
  points: number;
  birthDate: Date;
  photo: string;
  smallPhoto: string;
  isOnline: boolean;
  lastTimeOnline: Date;
  isConfirmed: boolean;
  isBanned: boolean;
  bannedUntil: Date;
  banReason: string;
  aboutYourself: string;
  roles: Role[];
  createdAt: Date;
  updatedAt: Date;
}

@DefaultScope(() => ({
  attributes: { exclude: ['password'] },
  exclude: ['posts'],
}))
@Scopes(() => ({
  withRole: {
    include: [
      {
        model: Role,
        attributes: {
          exclude: ['description', 'id'],
        },
        exclude: [{ model: UserRoles }],
        through: {
          attributes: [],
        },
      },
    ],
  },
  allUsers: {
    attributes: {
      exclude: [
        'password',
        'email',
        'birthDate',
        'isConfirmed',
        'aboutYourself',
      ],
    },
  },
}))
@Table({ tableName: 'users', createdAt: false, updatedAt: false })
export class User extends Model<UserAttrs, UserCreationAttrs> {
  @ApiProperty({ example: 1, description: 'Уникальный идентификатор' })
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @ApiProperty({
    example: 'Иван',
    description: 'Имя пользователя',
  })
  @Column({ type: DataType.STRING, allowNull: false })
  name: string;

  @ApiProperty({ example: 'user@mail.ru', description: 'Почтовый адрес' })
  @Column({ type: DataType.STRING, unique: true, allowNull: false })
  email: string;

  @Column({ type: DataType.STRING, allowNull: false })
  password: string;

  @ApiProperty({ example: 100, description: 'Кол-во очков' })
  @Column({
    type: DataType.INTEGER,
    defaultValue: 0,
  })
  points: number;

  @ApiProperty({ example: '2000-08-21', description: 'Дата рождения' })
  @Column({ type: DataType.DATEONLY, allowNull: true })
  birthDate: Date;

  @ApiProperty({ example: '/url', description: 'Фото' })
  @Column({ type: DataType.STRING, allowNull: true })
  photo: string;

  @ApiProperty({ example: '/url', description: 'Мал фото' })
  @Column({ type: DataType.STRING, allowNull: true })
  smallPhoto: string;

  @ApiProperty({ example: true, description: 'В сети или нет' })
  @Column({ type: DataType.BOOLEAN, defaultValue: false })
  isOnline: boolean;

  @ApiProperty({
    example: '2021-05-12T06:57:24.059Z',
    description: 'Последний раз в сети',
  })
  @Column({
    type: 'TIMESTAMP',
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
  })
  lastTimeOnline: Date;

  @ApiProperty({
    example: true,
    description: 'Подтвержден ли профиль по почте',
  })
  @Column({ type: DataType.BOOLEAN, defaultValue: false })
  isConfirmed: boolean;

  @ApiProperty({ example: true, description: 'Забанен или нет' })
  @Column({ type: DataType.BOOLEAN, defaultValue: false })
  isBanned: boolean;

  @ApiProperty({
    example: '2021-05-12T06:57:24.059Z',
    description: 'Забанен до',
  })
  @Column({
    type: 'TIMESTAMP',
    allowNull: true,
  })
  bannedUntil: Date;

  @ApiProperty({ example: 'Немного о себе', description: 'Инофрмация о себе' })
  @Column({ type: DataType.STRING, allowNull: true })
  aboutYourself: string;

  @ApiProperty({ example: 'За хулиганство', description: 'Причина блокировки' })
  @Column({ type: DataType.STRING, allowNull: true })
  banReason: string;

  @BelongsToMany(() => Role, () => UserRoles)
  roles: Role[];

  @ApiProperty({
    example: '2021-05-12T06:57:24.059Z',
    description: 'Время создания аккаунта',
  })
  @CreatedAt
  @Column({
    type: 'TIMESTAMP',
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    allowNull: true,
  })
  createdAt: Date;

  @ApiProperty({
    example: '2021-05-12T06:57:24.059Z',
    description: 'Время последнего обновления аккаунта',
  })
  @UpdatedAt
  @Column({
    type: 'TIMESTAMP',
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    allowNull: true,
  })
  updatedAt: Date;

  @HasMany(() => Post)
  posts: Post[];
}
