import {
  Column,
  DataType,
  ForeignKey,
  Model,
  Table,
} from 'sequelize-typescript';

// МОДЕЛИ БД
import { User } from '../users/users.model';
import { Ban } from './bans.model';

@Table({ tableName: 'userBans', createdAt: false, updatedAt: false })
export class UserBans extends Model<UserBans> {
  @Column({
    type: DataType.INTEGER,
    unique: true,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @ForeignKey(() => Ban)
  @Column({
    allowNull: true,
    onUpdate: 'CASCADE',
    onDelete: 'SET NULL',
  })
  roleId: number;

  @ForeignKey(() => User)
  @Column({
    allowNull: true,
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  userId: number;
}
