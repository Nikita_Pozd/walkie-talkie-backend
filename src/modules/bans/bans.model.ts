import {
  BelongsToMany,
  Column,
  DataType,
  DefaultScope,
  Model,
  Scopes,
  Table,
} from 'sequelize-typescript';
import { ApiProperty } from '@nestjs/swagger';

// МОДЕЛИ БД
import { User } from '../users/users.model';
import { UserBans } from './user-bans.model';

interface RoleCreationAttrs {
  value: string;
  description: string;
}

interface RoleAttrs {
  id: number;
  value: string;
  description: string;
}
@DefaultScope(() => ({
  attributes: { exclude: ['description'] },
}))
@Scopes(() => ({
  allBans: {
    attributes: {
      exclude: [],
    },
  },
}))
@Table({ tableName: 'bans', createdAt: false, updatedAt: false })
export class Ban extends Model<RoleAttrs, RoleCreationAttrs> {
  @ApiProperty({ example: 1, description: 'Уникальный идентификатор' })
  @Column({
    type: DataType.INTEGER,
    unique: true,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @ApiProperty({
    example: 'Причина бана',
    description: 'Уникальное причина блокировки',
  })
  @Column({ type: DataType.STRING, unique: true, allowNull: false })
  reason: string;

  @ApiProperty({
    example: 'Подробное описание',
    description: 'Подробное описание за что прилетает бан',
  })
  @Column({ type: DataType.STRING, allowNull: false })
  description: string;

  @BelongsToMany(() => User, () => UserBans)
  users: User[];
}
