import { ApiProperty } from '@nestjs/swagger';
import {
  BelongsTo,
  //   BelongsToMany,
  Column,
  CreatedAt,
  DataType,
  ForeignKey,
  Model,
  Sequelize,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';
import { User } from '../users/users.model';

interface PostCreationAttrs {
  title: string;
  content: string;
  userId: number;
  image: string;
}

interface PostAttrs {
  id: number;
  title: string;
  content: string;
  userId: number;
  image: string;
  createdAt: Date;
  updatedAt: Date;
  isHidden: boolean;
}

@Table({ tableName: 'posts', createdAt: false, updatedAt: false })
export class Post extends Model<PostAttrs, PostCreationAttrs> {
  @Column({
    type: DataType.INTEGER,
    unique: true,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @ApiProperty({
    example: 'Название поста',
    description: 'Название поста',
  })
  @Column({ type: DataType.STRING, unique: true, allowNull: false })
  title: string;

  @ApiProperty({
    example: 'Текст поста',
    description: 'Текст поста',
  })
  @Column({ type: DataType.STRING, allowNull: false })
  content: string;

  @ApiProperty({
    example: '/url',
    description: 'Ссылка на картинку',
  })
  @Column({ type: DataType.STRING })
  image: string;

  @ForeignKey(() => User)
  @Column
  userId: number;

  @BelongsTo(() => User)
  user: User;

  @ApiProperty({
    example: '2021-05-12T06:57:24.059Z',
    description: 'Время создания поста',
  })
  @CreatedAt
  @Column({
    type: 'TIMESTAMP',
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    allowNull: true,
  })
  createdAt: Date;

  @ApiProperty({
    example: '2021-05-12T06:57:24.059Z',
    description: 'Время последнего обновления поста',
  })
  @UpdatedAt
  @Column({
    type: 'TIMESTAMP',
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    allowNull: true,
  })
  updatedAt: Date;

  @ApiProperty({
    example: false,
    description: 'Скрыт ли пост',
  })
  @Column({ type: DataType.BOOLEAN, defaultValue: false })
  isHidden: boolean;
}
