'use strict';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const bcrypt = require('bcryptjs');

module.exports = {
  up: async (queryInterface) => {
    let roles = await queryInterface.bulkInsert(
      'roles',
      [
        {
          value: 'admin',
          description: 'Администратор',
        },
        {
          value: 'user',
          description: 'Пользователь',
        },
        {
          value: 'editor',
          description: 'Редактор',
        },
      ],
      { returning: true },
    );
    const salt = await bcrypt.genSalt(this.saltRounds);
    const hashPassword = await bcrypt.hash('password', salt);
    let users = await queryInterface.bulkInsert(
      'users',
      [
        {
          email: 'testAdmin@mail.ru',
          name: 'Никита',
          password: hashPassword,
          isConfirmed: true,
          aboutYourself: 'тестовый админ',
          birthDate: new Date(2000, 0, 1),
        },
        {
          email: 'testIvan@mail.ru',
          name: 'Иван',
          password: hashPassword,
          isConfirmed: true,
          aboutYourself: 'тестовый пользователь',
          birthDate: new Date(2000, 0, 1),
        },
        {
          email: 'testVasya@mail.ru',
          name: 'Василий',
          password: hashPassword,
          isConfirmed: true,
          aboutYourself: 'тестовый пользователь',
          birthDate: new Date(2000, 0, 1),
        },
      ],
      { returning: true },
    );
    let userRolesArr = [];
    users.forEach((val) => {
      if (val.email === 'testAdmin@mail.ru') {
        userRolesArr.push(
          {
            roleId: roles.find((val) => val.value === 'admin').id,
            userId: val.id,
          },
          {
            roleId: roles.find((val) => val.value === 'user').id,
            userId: val.id,
          },
        );
      } else {
        userRolesArr.push({
          roleId: roles.find((val) => val.value === 'user').id,
          userId: val.id,
        });
      }
    });
    await queryInterface.bulkInsert('userRoles', userRolesArr, {});
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('userRoles', null, {});
    await queryInterface.bulkDelete('roles', null, {});
    await queryInterface.bulkDelete('users', null, {});
  },
};
